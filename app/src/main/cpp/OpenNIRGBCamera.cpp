#include "OpenNIRGBCamera.h"

OpenNIRGBCamera::OpenNIRGBCamera(OpenNIContext *openNIContext) {
    this->openNIContext = openNIContext;
}

bool OpenNIRGBCamera::initVideoStream(int width, int height) {
    Device *device = openNIContext->getDevice();
    stream = new VideoStream();

    // create ir stream
    if (device->getSensorInfo(SENSOR_COLOR) != nullptr) {
        Status rc = stream->create(*device, SENSOR_COLOR);
        if (rc != STATUS_OK) {
            LOGE("Couldn't create Color stream\n%s", OpenNI::getExtendedError());
            return false;
        }
    }

    // set video mode
    const SensorInfo &sensorInfo = *device->getSensorInfo(SENSOR_COLOR);
    const Array<VideoMode> &modes = sensorInfo.getSupportedVideoModes();

    for (int i = 0; i < modes.getSize(); ++i) {
        const VideoMode &mode = modes[i];
        LOGI("Color mode %dx%d %d", mode.getResolutionX(), mode.getResolutionY(),
             mode.getPixelFormat());
        if (mode.getPixelFormat() == PIXEL_FORMAT_RGB888 &&
            mode.getResolutionX() == width &&
            mode.getResolutionY() == height) {
            stream->setVideoMode(mode);
            break;
        }
    }

    Status rc = stream->start();
    if (rc != STATUS_OK) {
        LOGE("Couldn't start the Color stream\n%s", OpenNI::getExtendedError());
        return false;
    }

    LOGI("Color stream started");
    videoFrameRef = new VideoFrameRef();
    return true;
}

int OpenNIRGBCamera::readFrame(Mat &frame) {
    OniRGB888Pixel *buf;
    Status rc = stream->readFrame(videoFrameRef);

    if (rc != STATUS_OK) {
        LOGE("Read failed!\n%s", OpenNI::getExtendedError());
        return -1;
    }

    buf = (OniRGB888Pixel *) videoFrameRef->getData();
    frame = Mat(videoFrameRef->getHeight(), videoFrameRef->getWidth(), CV_8UC3, buf);
    return 0;
}

void OpenNIRGBCamera::convertGrayScale(Mat &in, Mat &out) {
    cvtColor(in, out, CV_RGB2GRAY);
}

void OpenNIRGBCamera::convertRGBA(Mat &in, Mat &out) {
    out = in;
//    cvtColor(in, out, CV_BGR2RGBA);
}
