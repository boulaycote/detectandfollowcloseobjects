#ifndef DETECTANDFOLLOWCLOSEOBJECTS_OPENNIRGBCAMERA_H
#define DETECTANDFOLLOWCLOSEOBJECTS_OPENNIRGBCAMERA_H

#include "OpenNICamera.h"

using namespace openni;
using namespace cv;


class OpenNIRGBCamera : virtual public OpenNICamera {

public:

    OpenNIRGBCamera(OpenNIContext *openNIContext);

    bool initVideoStream(int width, int height);

    int readFrame(Mat &frame);

    void convertGrayScale(Mat &in, Mat &out);

    void convertRGBA(Mat &in, Mat &out);
};


#endif //DETECTANDFOLLOWCLOSEOBJECTS_OPENNIRGBCAMERA_H
