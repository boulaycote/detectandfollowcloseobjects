#include "IRCamera.h"

IRCamera::IRCamera(OpenNIContext *openNIContext) {
    this->openNIContext = openNIContext;
}

bool IRCamera::initVideoStream(int width, int height) {
    Device *device = openNIContext->getDevice();
    stream = new VideoStream();

    // create ir stream
    if (device->getSensorInfo(SENSOR_IR) != nullptr) {
        Status rc = stream->create(*device, SENSOR_IR);
        if (rc != STATUS_OK) {
            LOGE("Couldn't create IR stream\n%s", OpenNI::getExtendedError());
            return false;
        }
    }

    // set video mode
    const SensorInfo &sensorInfo = *device->getSensorInfo(SENSOR_IR);
    const Array<VideoMode> &modes = sensorInfo.getSupportedVideoModes();

    for (int i = 0; i < modes.getSize(); ++i) {
        const VideoMode &mode = modes[i];
        LOGI("IR mode %dx%d %d", mode.getResolutionX(), mode.getResolutionY(),
             mode.getPixelFormat());
        if (mode.getPixelFormat() == PIXEL_FORMAT_RGB888 &&
            mode.getResolutionX() == width &&
            mode.getResolutionY() == height) {
            stream->setVideoMode(mode);
            break;
        }
    }

    Status rc = stream->start();
    if (rc != STATUS_OK) {
        LOGE("Couldn't start the IR stream\n%s", OpenNI::getExtendedError());
        return false;
    }

    LOGI("IR stream started");
    videoFrameRef = new VideoFrameRef();
    return true;
}

int IRCamera::readFrame(Mat &frame) {
    OniRGB888Pixel *buf;
    Status rc = stream->readFrame(videoFrameRef);

    if (rc != STATUS_OK) {
        LOGE("Read failed!\n%s", OpenNI::getExtendedError());
        return -1;
    }

    buf = (OniRGB888Pixel *) videoFrameRef->getData();
    frame = Mat(videoFrameRef->getHeight(), videoFrameRef->getWidth(), CV_8UC3, buf);
    return 0;
}

void IRCamera::convertGrayScale(Mat &in, Mat &out) {
    cvtColor(in, out, CV_BGR2GRAY);
}

void IRCamera::convertRGBA(Mat &in, Mat &out) {
    cvtColor(in, out, CV_BGR2RGBA);
}
