#include "OpenNICamera.h"


bool OpenNICamera::initVideoStream(int width, int height) {}

int OpenNICamera::init(int width, int height) {
    if (initVideoStream(width, height)) {
        return 0;
    }
    return -1;
}

void OpenNICamera::release() {
    // Somehow, releasing this causes a crash...
//    if (videoFrameRef != nullptr) {
//        videoFrameRef->release();
//        videoFrameRef = nullptr;
//    }

    // This should be released when the device is closed.
//    if (stream != nullptr) {
//        stream->stop();
//        stream->destroy();
//        stream = nullptr;
//    }
}

int OpenNICamera::readFrame(Mat &frame) {}
