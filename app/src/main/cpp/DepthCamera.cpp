#include "DepthCamera.h"

DepthCamera::DepthCamera(OpenNIContext *openNIContext) {
    this->openNIContext = openNIContext;
}

bool DepthCamera::initVideoStream(int width, int height) {
    Device *device = openNIContext->getDevice();
    stream = new openni::VideoStream();

    // create stream
    if (device->getSensorInfo(openni::SENSOR_DEPTH) != nullptr) {
        openni::Status rc = stream->create(*device, openni::SENSOR_DEPTH);

        if (rc != openni::STATUS_OK) {
            LOGE("Couldn't create Depth stream\n%s", openni::OpenNI::getExtendedError());
            return false;
        }
    }

    // set video mode
    const openni::SensorInfo &sensorInfo = *device->getSensorInfo(openni::SENSOR_DEPTH);
    const openni::Array<openni::VideoMode> &modes = sensorInfo.getSupportedVideoModes();

    for (int i = 0; i < modes.getSize(); ++i) {
        const openni::VideoMode &mode = modes[i];
        LOGI("Depth mode %dx%d %d", mode.getResolutionX(), mode.getResolutionY(),
             mode.getPixelFormat());

//        PIXEL_FORMAT_DEPTH_1_MM  1mm
//        PIXEL_FORMAT_DEPTH_100_UM 100 micro metre

        if (mode.getPixelFormat() == openni::PIXEL_FORMAT_DEPTH_1_MM &&
            mode.getResolutionX() == width &&
            mode.getResolutionY() == height) {
            stream->setVideoMode(mode);
            break;
        }
    }

    openni::Status rc = stream->start();
    if (rc != openni::STATUS_OK) {
        LOGE("Couldn't start the Depth stream\n%s", openni::OpenNI::getExtendedError());
        return false;
    }

    LOGI("Depth stream started");
    videoFrameRef = new openni::VideoFrameRef();
    return true;
}

int DepthCamera::readFrame(Mat &frame) {
    OniDepthPixel *buf;
    openni::Status rc = stream->readFrame(videoFrameRef);

    if (rc != openni::STATUS_OK) {
        LOGE("Read failed!\n%s", openni::OpenNI::getExtendedError());
        return -1;
    }

    buf = (OniDepthPixel *) videoFrameRef->getData();
    frame = Mat(videoFrameRef->getHeight(), videoFrameRef->getWidth(), CV_16UC1, buf);

    return 0;
}

void DepthCamera::convertGrayScale(Mat &in, Mat &out) {
    double min, max;
    double scale;

    minMaxIdx(in, &min, &max);
    scale = 255 / (max - min);
    in.convertTo(out, CV_8UC1, scale, -min * scale);
}

void DepthCamera::convertRGBA(Mat &in, Mat &out) {
    convertGrayScale(in, out);
    cvtColor(out, out, CV_GRAY2BGRA);
}