#include "OpenNIContext.h"

Device *OpenNIContext::getDevice() {
    if (maybeInitOpenNI() && maybeInitDevice()) {
        return device;
    }
    return nullptr;
}

void OpenNIContext::release() {
    if (device != nullptr && deviceReady) {
        device->close();
        deviceReady = false;
    }
    if (openNIReady) {
        OpenNI::shutdown();
        openNIReady = false;
    }
    LOGI("Context released");
}

bool OpenNIContext::maybeInitOpenNI() {
    if (!openNIReady) {
        openNIReady = initOpenNI();
    }
    return openNIReady;
}

bool OpenNIContext::initOpenNI() {
    Status rc = OpenNI::initialize();

    if (rc != STATUS_OK) {
        LOGE("Initialize failed\n%s", OpenNI::getExtendedError());
        return false;
    }
    return true;
}

bool OpenNIContext::maybeInitDevice() {
    if (!deviceReady) {
        deviceReady = initDevice();
    }
    return deviceReady;
}

bool OpenNIContext::initDevice() {
    device = new openni::Device();
    Status rc = device->open(ANY_DEVICE);

    if (rc != STATUS_OK) {
        LOGE("Couldn't open device\n%s", OpenNI::getExtendedError());
        return false;
    }
    logDeviceSerialNumber();
    logDeviceName();
    return true;
}


void OpenNIContext::logDeviceName() const {
    char name[20];
    int dataSize = sizeof(name);

    memset(name, 0, dataSize);
    device->getProperty(openni::OBEXTENSION_ID_DEVICETYPE, (uint8_t *) &name, &dataSize);
    LOGI("device name %s", name);
}

void OpenNIContext::logDeviceSerialNumber() const {
    char serialNumber[12];
    int dataSize = sizeof(serialNumber);

    memset(serialNumber, 0, dataSize);
    device->getProperty(openni::OBEXTENSION_ID_SERIALNUMBER, (uint8_t *) &serialNumber, &dataSize);
    LOGI("device serial number %s", serialNumber);
}