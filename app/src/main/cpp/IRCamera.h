#ifndef DETECTANDFOLLOWCLOSEOBJECTS_IRCAMERA_H
#define DETECTANDFOLLOWCLOSEOBJECTS_IRCAMERA_H

#include "OpenNICamera.h"

using namespace openni;
using namespace cv;


class IRCamera : virtual public OpenNICamera {

public:

    IRCamera(OpenNIContext *openNIContext);

    bool initVideoStream(int width, int height);

    int readFrame(Mat &frame);

    void convertGrayScale(Mat &in, Mat &out);

    void convertRGBA(Mat &in, Mat &out);
};

#endif //DETECTANDFOLLOWCLOSEOBJECTS_IRCAMERA_H
