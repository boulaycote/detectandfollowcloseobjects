#ifndef DETECTANDFOLLOWCLOSEOBJECTS_LOG_H
#define DETECTANDFOLLOWCLOSEOBJECTS_LOG_H

#include <jni.h>
#include <android/log.h>

#define LOG_TAG "Camera-Jni"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#define ERROR_LOCAL -1
#define SUCCESS_LOCAL 0

#endif //DETECTANDFOLLOWCLOSEOBJECTS_LOG_H
