#include "TrackingCamera.h"
#include "OpenNIRGBCamera.h"

const Scalar BLACK = Scalar(0.0, 0.0, 0.0);
const Scalar WHITE = Scalar(255.0, 255.0, 255.0);

TrackingCamera::TrackingCamera(OpenNIContext *openNIContext) {
    depthCamera = new DepthCamera(openNIContext);
    irCamera = new IRCamera(openNIContext);
    rgbCamera = new OpenNIRGBCamera(openNIContext);
//    rgbCamera = new RGBCamera();
}

int TrackingCamera::init(const char *deviceName, int width, int height) {
    int res;

    res = depthCamera->init(width, height);

    if (res == SUCCESS_LOCAL) {
        res = irCamera->init(width, height);
    }

//    if (res == SUCCESS_LOCAL) {
//        res = rgbCamera->init(deviceName, width, height);
//        res = rgbCamera->init(width, height);
//    }

//    subtractor = createBackgroundSubtractorMOG2();
    blobTracker = new BlobTracker();

    return res;
}

void TrackingCamera::release() {
    if (depthCamera != nullptr) {
        depthCamera->release();
    }
    if (irCamera != nullptr) {
        irCamera->release();
    }
    if (rgbCamera != nullptr) {
        rgbCamera->release();
    }
}

int TrackingCamera::readFrame(Mat &frame, bool &objectFound) {
//    Mat rgb, ir, depth, full;
//
//    rgbCamera->readFrame(rgb);
//    rgbCamera->convertRGBA(rgb, rgb);
//    full.push_back(rgb);
//    irCamera->readFrame(ir);
//    irCamera->convertRGBA(ir, ir);
//    full.push_back(ir);
//    depthCamera->readFrame(depth);
//    depthCamera->convertRGBA(depth, depth);
//    frame = depth;
//    full.push_back(depth);

//    frame = full;

    // Simple object detection
    Mat rgb, depth, ir, gray;
    vector<DetectedObject> detectedObjects;

//    rgbCamera->readFrame(rgb);
//    rgbCamera->convertRGBA(rgb, rgb);
    depthCamera->readFrame(depth);
    irCamera->readFrame(ir);
    irCamera->convertGrayScale(ir, gray);

    objectFound = detectObjects(ir, gray, depth, detectedObjects);

//    frame = rgb;
    cvtColor(ir, frame, CV_BGR2RGBA);

    // Background subtraction algorithm.
//    Mat ir, depth;
//
//    irCamera->readFrame(ir);
//    depthCamera->readFrame(depth);
//
//    subtractor->apply(ir, mask);
//    morphologyEx(mask, mask, MORPH_OPEN, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));
//
//    cvtColor(mask, frame, CV_GRAY2BGRA);

    // Block tracking
//    frame = trackObjects(irCamera);
//
//    cvtColor(frame, frame, CV_GRAY2BGRA);

    // RGB Camera
//    Mat rgb;
//
//    rgbCamera->readFrame(rgb);
//
//    frame = rgb;

    return SUCCESS_LOCAL;
}

Mat TrackingCamera::trackObjects(OpenNICamera *camera) {
    Mat frame1, frame2, gray1, gray2, diff, thresh;
    vector<vector<Point>> contours;
    Mat kernel5x5 = getStructuringElement(MORPH_RECT, Size(5, 5));

    camera->readFrame(frame1);
    camera->readFrame(frame2);
    camera->convertGrayScale(frame1, gray1);
    camera->convertGrayScale(frame2, gray2);

    GaussianBlur(gray1, gray1, Size(5, 5), 0);
    GaussianBlur(gray2, gray2, Size(5, 5), 0);

    absdiff(gray1, gray2, diff);

    threshold(diff, thresh, 15, 255, THRESH_BINARY);

    dilate(thresh, thresh, kernel5x5);
    dilate(thresh, thresh, kernel5x5);
    erode(thresh, thresh, kernel5x5);

    findContours(thresh, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

    vector<vector<Point>> convexHulls(contours.size());

    for (int i = 0; i < contours.size(); i++) {
        convexHull(contours[i], convexHulls[i]);
    }

    Mat image(thresh.size(), CV_8UC1, BLACK);
    drawContours(image, convexHulls, -1, WHITE, -1);

    return image;
}

bool TrackingCamera::detectObjects(Mat &original, Mat &gray, Mat &depth,
                                   vector<DetectedObject> detectedObjects) {
    int scale = 2;
    Mat work, labels, stats, centroids;

    // downscale for faster operations
    resize(gray, work, Size(gray.rows / scale, gray.cols / scale));

    // TODO parameterize value = lower threshold.
    threshold(work, work, 11, 255, THRESH_BINARY);

    erode(work, work, getStructuringElement(MORPH_RECT, Size(5, 5)));
    dilate(work, work, getStructuringElement(MORPH_RECT, Size(5, 5)));

    // rescale for precise locations
    resize(work, work, Size(work.rows * scale, work.cols * scale));

    int count = connectedComponentsWithStats(work, labels, stats, centroids);

    // TODO parameterize
    const int MIN_SIZE = (gray.rows * gray.cols) / 20;
    int id = 1;

    for (int i = 1; i < count; i++) {
        int x = stats.at<int>(i, CC_STAT_LEFT);
        int y = stats.at<int>(i, CC_STAT_TOP);
        int w = stats.at<int>(i, CC_STAT_WIDTH);
        int h = stats.at<int>(i, CC_STAT_HEIGHT);
        int size = stats.at<int>(i, CC_STAT_AREA);

        if (size > MIN_SIZE) {
            Rect bounds = Rect(x, y, w, h);
            Mat roi = depth(bounds);
            Mat mask = roi > 0; // Remove all 0s
            char text[255];
            double min, max;

            minMaxIdx(depth(bounds), &min, &max, NULL, NULL, mask);

            detectedObjects.push_back(DetectedObject(id, x, y, w, h, min));
            sprintf(text, "%d dist: %g", id++, min);

            putText(original, text, Point(x + 20, y + 30), 1, 1.5, Scalar(100, 255, 100), 2);
            rectangle(original, bounds, Scalar(100, 255, 100), 2);
            circle(original, Point(x + w / 2, y + h / 2), 2, Scalar(100, 255, 100), 2);
        }
    }

    return detectedObjects.size() > 0;
}

//    vector<vector<Point>> contours;
//    vector<Vec4i> hierarchy;

//    findContours(work, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

//    double refArea = 0;
//    bool objectFound = false;
//    int x, y;
//    const int MIN_OBJECT_AREA = 20 * 20;
//
//    if (hierarchy.size() > 0) {
//        int objectsCount = hierarchy.size();
//        if (objectsCount < 50) {
//            for (int index = 0; index >= 0; index = hierarchy[index][0]) {
//                Moments moment = moments(contours[index]);
//                double area = moment.m00;
//
//                if (area > MIN_OBJECT_AREA && area > refArea) {
//                    x = moment.m10 / area;
//                    y = moment.m01 / area;
//
//                    // TODO Later, we'll detect more stuff...
//                    points.push_back(Point(x, y));
//
//                    objectFound = true;
//                    refArea = area;
//                } else {
//                    objectFound = false;
//                }
//            }
//            return objectFound;
//        }
//    }
//    return false;
