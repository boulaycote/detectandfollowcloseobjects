#include <jni.h>
#include <android/log.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/tracking.hpp>

using namespace cv;

class SimpleTracker {
public:
    void update(Mat mat);

    void release();

    SimpleTracker () {
        initialized = false;
    }

private:
    Ptr<Tracker> tracker;
    bool initialized;
    Rect2d boundingBox;
};

void SimpleTracker::update(Mat mat) {
    if (!initialized) {
        tracker = TrackerKCF::create();
        boundingBox = Rect2d(287, 23, 86, 320);
//        boundingBox = selectROI(mat, false);
        tracker->init(mat, boundingBox);
        initialized = true;
    } else {
        if (tracker->update(mat, boundingBox)) {
            rectangle(mat, boundingBox, Scalar(255, 0, 0), 2, 1);
        } else {
            putText(mat, "Tracking failure detected", Point(100, 80), FONT_HERSHEY_SIMPLEX, 0.75,
                    Scalar(0, 0, 255), 2);
        }
    }
}

void SimpleTracker::release() {

}
