#ifndef DETECTANDFOLLOWCLOSEOBJECTS_OPENNICONTEXT_H
#define DETECTANDFOLLOWCLOSEOBJECTS_OPENNICONTEXT_H

#include "Log.h"
#include <opencv2/opencv.hpp>
#include "Include/OpenNI.h"
#include "Include/OniCAPI.h"

using namespace openni;
using namespace cv;

class OpenNIContext {

    bool openNIReady;

    bool deviceReady;

    Device *device;

    bool maybeInitOpenNI();

    bool maybeInitDevice();

    bool initOpenNI();

    bool initDevice();

    void logDeviceName() const;

    void logDeviceSerialNumber() const;

public:
    OpenNIContext() {}

    Device *getDevice();

    void release();
};


#endif //DETECTANDFOLLOWCLOSEOBJECTS_OPENNICONTEXT_H
