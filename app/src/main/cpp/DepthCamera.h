#ifndef DETECTANDFOLLOWCLOSEOBJECTS_DEPTHCAMERA_H
#define DETECTANDFOLLOWCLOSEOBJECTS_DEPTHCAMERA_H

#include "OpenNICamera.h"

using namespace openni;
using namespace cv;


class DepthCamera : virtual public OpenNICamera {

public:

    DepthCamera(OpenNIContext *openNIContext);

    bool initVideoStream(int width, int height);

    int readFrame(Mat &frame);

    void convertGrayScale(Mat &in, Mat &out);

    void convertRGBA(Mat &in, Mat &out);
};

#endif //DETECTANDFOLLOWCLOSEOBJECTS_DEPTHCAMERA_H
