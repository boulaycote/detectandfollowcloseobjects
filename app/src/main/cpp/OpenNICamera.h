#ifndef DETECTANDFOLLOWCLOSEOBJECTS_OPENNICAMERA_H
#define DETECTANDFOLLOWCLOSEOBJECTS_OPENNICAMERA_H

#include "Log.h"
#include <opencv2/opencv.hpp>
#include "Include/OpenNI.h"
#include "Include/OniCAPI.h"
#include "OpenNIContext.h"

using namespace openni;
using namespace cv;


class OpenNICamera {

protected:

    OpenNIContext *openNIContext;
    VideoStream *stream;
    VideoFrameRef *videoFrameRef;

    virtual bool initVideoStream(int width, int height) = 0;

public:
    int init(int width, int height);

    void release();

    virtual int readFrame(Mat &frame) = 0;

    virtual void convertGrayScale(Mat &in, Mat &out) = 0;

    virtual void convertRGBA(Mat &in, Mat &out) = 0;
};

#endif //DETECTANDFOLLOWCLOSEOBJECTS_OPENNICAMERA_H
