#ifndef DETECTANDFOLLOWCLOSEOBJECTS_DEVICE_H
#define DETECTANDFOLLOWCLOSEOBJECTS_DEVICE_H

#include "../Log.h"
#include "Utils.h"
#include <sys/stat.h>
#include <linux/videodev2.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <malloc.h>
#include <sys/mman.h>

typedef struct {
    void *start;
    size_t length;
} Buffer;

class VideoDevice {
private:

    int fd;
    int bufferCount;
    Buffer *buffers;

    void enumerateControls();

    void enumerateMenu(struct v4l2_queryctrl control);

    int initMMap();

    void configureControl(__u32 id, __u32 value);

    void processCamera(void **bufferAddr);

    int processFrame(void **bufferAddr);

public:

    VideoDevice();

    int openDevice(const char *deviceName);

    int configureDevice(int width, int height);

    int start();

    int stop();

    int closeDevice();

    int release();

    void readFrame(void **bufferAddr);

    bool opened();
};


#endif //DETECTANDFOLLOWCLOSEOBJECTS_DEVICE_H
