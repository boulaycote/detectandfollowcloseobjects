#include <unistd.h>
#include <assert.h>
#include "VideoDevice.h"

VideoDevice::VideoDevice() {
    this->fd = -1;
}

int VideoDevice::openDevice(const char *deviceName) {
    struct stat st;

    LOGI("Opening device '%s'", deviceName);

    if (-1 == stat(deviceName, &st)) {
        LOGE("Cannot identify '%s': %d, %s", deviceName, errno, strerror(errno));
        return ERROR_LOCAL;
    }

    if (!S_ISCHR(st.st_mode)) {
        LOGE("%s is not a valid device", deviceName);
        return ERROR_LOCAL;
    }

    fd = open(deviceName, O_RDWR | O_NONBLOCK, 0);
    if (-1 == fd) {
        LOGE("Cannot open '%s': %d, %s", deviceName, errno, strerror(errno));
        if (EACCES == errno) {
            LOGE("Insufficient permissions on '%s': %d, %s", deviceName, errno, strerror(errno));
        }
        return ERROR_LOCAL;
    }

    return SUCCESS_LOCAL;
}

int VideoDevice::configureDevice(int width, int height) {
    struct v4l2_capability cap;
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;
    struct v4l2_format format;
    unsigned int min;

    if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &cap)) {
        if (EINVAL == errno) {
            LOGE("not a valid V4L2 device");
            return ERROR_LOCAL;
        } else {
            return errnoexit("VIDIOC_QUERYCAP");
        }
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        LOGE("device is not a video capture device");
        return ERROR_LOCAL;
    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        LOGE("device does not support streaming i/o");
        return ERROR_LOCAL;
    }

    CLEAR(cropcap);
    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (0 == xioctl(fd, VIDIOC_CROPCAP, &cropcap)) {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect;

        if (-1 == xioctl(fd, VIDIOC_S_CROP, &crop)) {
            switch (errno) {
                case EINVAL:
                    break;
                default:
                    break;
            }
        }
    }

    enumerateControls();

//    configureControl(V4L2_CID_AUTO_WHITE_BALANCE, -1);
//    configureControl(V4L2_CID_BACKLIGHT_COMPENSATION, -1);
//    configureControl(V4L2_CID_GAIN, -1);
//    configureControl(V4L2_CID_EXPOSURE_AUTO, V4L2_EXPOSURE_MANUAL);

    CLEAR(format);
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    format.fmt.pix.width = width;
    format.fmt.pix.height = height;

    format.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    format.fmt.pix.field = V4L2_FIELD_INTERLACED;

    if (-1 == xioctl(fd, VIDIOC_S_FMT, &format)) {
        return errnoexit("VIDIOC_S_FMT");
    }

    min = format.fmt.pix.width * 2;
    if (format.fmt.pix.bytesperline < min) {
        format.fmt.pix.bytesperline = min;
    }

    min = format.fmt.pix.bytesperline * format.fmt.pix.height;
    if (format.fmt.pix.sizeimage < min) {
        format.fmt.pix.sizeimage = min;
    }

    return initMMap();
}

int VideoDevice::start() {
    unsigned int i;
    enum v4l2_buf_type type;

    for (i = 0; i < bufferCount; ++i) {
        struct v4l2_buffer buf;
        CLEAR(buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        if (-1 == xioctl(fd, VIDIOC_QBUF, &buf)) {
            return errnoexit("VIDIOC_QBUF");
        }
    }

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(fd, VIDIOC_STREAMON, &type)) {
        return errnoexit("VIDIOC_STREAMON");
    }

    return SUCCESS_LOCAL;
}

int VideoDevice::stop() {
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (-1 != fd && -1 == xioctl(fd, VIDIOC_STREAMOFF, &type)) {
        return errnoexit("VIDIOC_STREAMOFF");
    }

    for (unsigned int i = 0; i < bufferCount; ++i) {
        if (-1 == munmap(buffers[i].start, buffers[i].length)) {
            return errnoexit("munmap");
        }
    }
    free(buffers);

    if (-1 != fd && -1 == close(fd)) {
        errnoexit("close");
    }
    fd = -1;

    return SUCCESS_LOCAL;
}

void VideoDevice::readFrame(void **bufferAddr) {
    processCamera(bufferAddr);
}

bool VideoDevice::opened() {
    return fd != -1;
}

void VideoDevice::configureControl(__u32 id, __u32 value) {
    struct v4l2_queryctrl queryctrl;
    struct v4l2_control control;

    CLEAR(queryctrl);

    queryctrl.id = id;
    if (-1 == xioctl(fd, VIDIOC_QUERYCTRL, &queryctrl)) {
        if (errno != EINVAL) {
            LOGE("VIDIOC_QUERYCTRL");
        } else {
            LOGI("Control %i is not supported\n", id);
        }
    } else if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
        LOGI("Control %s is not supported\n", queryctrl.name);
    } else {
        CLEAR(control);
        control.id = id;

        if (value < 0) {
            value = queryctrl.default_value;
        }
        control.value = value;

        if (-1 == xioctl(fd, VIDIOC_S_CTRL, &control)) {
            LOGE("VIDIOC_S_CTRL");
        } else {
            LOGI("Control %s set to %i\n", queryctrl.name, value);
        }
    }
}

int VideoDevice::closeDevice() {
    int result = SUCCESS_LOCAL;
    if (-1 != fd && -1 == close(fd)) {
        result = errnoexit("close");
    }
    fd = -1;
    return result;
}

int VideoDevice::release() {
    for (int i = 0; i < bufferCount; ++i) {
        if (-1 == munmap(buffers[i].start, buffers[i].length)) {
            return errnoexit("munmap");
        }
    }
    free(buffers);
    return SUCCESS_LOCAL;
}

int VideoDevice::initMMap() {
    struct v4l2_requestbuffers req;

    CLEAR(req);
    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            LOGE("device does not support memory mapping");
            return ERROR_LOCAL;
        } else {
            return errnoexit("VIDIOC_REQBUFS");
        }
    }

    if (req.count < 2) {
        LOGE("Insufficient buffer memory");
        return ERROR_LOCAL;
    }

    buffers = new Buffer[req.count];
    if (!buffers) {
        LOGE("Out of memory");
        return ERROR_LOCAL;
    }

    bufferCount = req.count;

    for (__u32 i = 0; i < req.count; ++i) {
        struct v4l2_buffer buf;
        CLEAR(buf);

        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        if (-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf)) {
            return errnoexit("VIDIOC_QUERYBUF");
        }

        buffers[i].length = buf.length;
        buffers[i].start = mmap(NULL, buf.length,
                                PROT_READ | PROT_WRITE, MAP_SHARED, fd,
                                buf.m.offset);

        if (MAP_FAILED == buffers[i].start) {
            return errnoexit("mmap");
        }
    }

    return SUCCESS_LOCAL;
}

void VideoDevice::enumerateControls() {
    struct v4l2_queryctrl control;

    LOGI("Available Video Device options");

    CLEAR(control);

    for (control.id = V4L2_CID_BASE;
         control.id < V4L2_CID_LASTP1;
         control.id++) {
        if (0 == ioctl(fd, VIDIOC_QUERYCTRL, &control)) {
            if (control.flags & V4L2_CTRL_FLAG_DISABLED)
                continue;

            LOGI("- Control %s [%i..%i]\n", control.name, control.minimum, control.maximum);

            if (control.type == V4L2_CTRL_TYPE_MENU)
                enumerateMenu(control);
        } else {
            if (errno == EINVAL)
                continue;

            LOGE("VIDIOC_QUERYCTRL");
        }
    }

    for (control.id = V4L2_CID_PRIVATE_BASE;;
         control.id++) {
        if (0 == xioctl(fd, VIDIOC_QUERYCTRL, &control)) {
            if (control.flags & V4L2_CTRL_FLAG_DISABLED)
                continue;

            LOGI("Control %s\n", control.name);

            if (control.type == V4L2_CTRL_TYPE_MENU)
                enumerateMenu(control);
        } else {
            if (errno == EINVAL)
                break;

            LOGE("VIDIOC_QUERYCTRL");
        }
    }
}

void VideoDevice::enumerateMenu(struct v4l2_queryctrl control) {
    struct v4l2_querymenu menu;

    CLEAR(menu);
    menu.id = control.id;

    for (menu.index = control.minimum;
         menu.index <= control.maximum;
         menu.index++) {
        if (0 == xioctl(fd, VIDIOC_QUERYMENU, &menu)) {
            LOGI("  - %s\n", menu.name);
        } else {
            LOGE("VIDIOC_QUERYMENU");
        }
    }
}

void VideoDevice::processCamera(void **bufferAddr) {
    if (fd == -1) {
        return;
    }

    for (;;) {
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(fd, &fds);

        struct timeval tv;
        tv.tv_sec = 2;
        tv.tv_usec = 0;

        int result = select(fd + 1, &fds, NULL, NULL, &tv);
        if (-1 == result) {
            if (EINTR == errno) {
                continue;
            }
            errnoexit("select");
        } else if (0 == result) {
            LOGE("select timeout");
        }

        if (processFrame(bufferAddr) == 1) {
            break;
        }
    }
}

int VideoDevice::processFrame(void **bufferAddr) {
    struct v4l2_buffer buf;

    CLEAR(buf);
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
        switch (errno) {
            case EAGAIN:
                return 0;
            case EIO:
            default:
                return errnoexit("VIDIOC_DQBUF");
        }
    }

    assert(buf.index < bufferCount);
    *bufferAddr = buffers[buf.index].start;

    if (-1 == xioctl(fd, VIDIOC_QBUF, &buf)) {
        return errnoexit("VIDIOC_QBUF");
    }

    return 1;
}