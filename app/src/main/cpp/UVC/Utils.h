#ifndef DETECTANDFOLLOWCLOSEOBJECTS_UTILS_H
#define DETECTANDFOLLOWCLOSEOBJECTS_UTILS_H

#include "../Log.h"
#include <sys/ioctl.h>
#include <string.h>
#include <errno.h>

#define CLEAR(x) memset(&(x), 0, sizeof(x))

int errnoexit(const char *s);

int xioctl(int fd, int request, void *arg);


#endif //DETECTANDFOLLOWCLOSEOBJECTS_UTILS_H
