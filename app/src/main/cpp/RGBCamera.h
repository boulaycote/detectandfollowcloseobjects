#ifndef DETECTANDFOLLOWCLOSEOBJECTS_RGBCAMERA_H
#define DETECTANDFOLLOWCLOSEOBJECTS_RGBCAMERA_H

#include "Log.h"
#include <opencv2/opencv.hpp>
#include "Include/OpenNI.h"
#include "Include/OniCAPI.h"
#include "UVC/VideoDevice.h"

using namespace openni;
using namespace cv;

class RGBCamera {

    VideoDevice *videoDevice;

    int width;

    int height;

    unsigned char *imageBuffer;

    bool initDevice(const char *deviceName, int width, int height);

    void logDeviceName() const;

    void logDeviceSerialNumber() const;

    bool initVideoStream(int width, int height);

public:

    RGBCamera();

    int init(const char *deviceName, int width, int height);

    void release();

    int readFrame(Mat &frame);

    void convertGrayScale(Mat &in, Mat &out);

    void convertRGBA(Mat &in, Mat &out);
};


#endif //DETECTANDFOLLOWCLOSEOBJECTS_RGBCAMERA_H
