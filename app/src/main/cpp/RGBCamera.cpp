#include "RGBCamera.h"

RGBCamera::RGBCamera() {
    videoDevice = new VideoDevice();
}

bool RGBCamera::initDevice(const char *deviceName, int width, int height) {
    int result = videoDevice->openDevice(deviceName);

    this->width = width;
    this->height = height;

    if (result != SUCCESS_LOCAL) {
        return false;
    }

    // TODO configure size and all...
    result = videoDevice->configureDevice(width, height);
    if (result != SUCCESS_LOCAL) {
        return false;
    }
    return result == SUCCESS_LOCAL;
}

void RGBCamera::logDeviceName() const {

}

void RGBCamera::logDeviceSerialNumber() const {

}

bool RGBCamera::initVideoStream(int width, int height) {
    int result = videoDevice->start();

    if (result != SUCCESS_LOCAL) {
        videoDevice->stop();
        LOGE("Unable to start capture, resetting device");
    } else {
        imageBuffer = new unsigned char[width * height];
    }
    return result == SUCCESS_LOCAL;
}

int RGBCamera::init(const char *deviceName, int width, int height) {
    if (initDevice(deviceName, width, height) && initVideoStream(width, height)) {
        logDeviceName();
        logDeviceSerialNumber();
        return SUCCESS_LOCAL;
    }
    return ERROR_LOCAL;
}

void RGBCamera::release() {
    if (imageBuffer) {
        free(imageBuffer);
    }
    videoDevice->stop();
    videoDevice->closeDevice();
    videoDevice->release();
}

int RGBCamera::readFrame(Mat &frame) {
    videoDevice->readFrame((void **) &imageBuffer);

    frame = Mat(height, width, CV_8UC2, imageBuffer);

    return SUCCESS_LOCAL;
}

void RGBCamera::convertGrayScale(Mat &in, Mat &out) {
    cvtColor(in, out, CV_RGBA2GRAY);
}

void RGBCamera::convertRGBA(Mat &in, Mat &out) {
    cvtColor(in, out, CV_YUV2RGBA_YUYV);
}
