#include <jni.h>
#include "Log.h"
#include "OpenNIContext.h"
#include "OpenNICamera.h"
#include "TrackingCamera.h"

#define REGISTER_CLASS "com/axis4/android/jni/ObjectTracker"

OpenNIContext *openNIContext;
TrackingCamera *trackingCamera;

int _erodeBy = -1;
int _dilateBy = -1;
int _blurBy = -1;

jint init(JNIEnv *env, jobject instance, jstring deviceName) {
    int result;
    const char *dev = env->GetStringUTFChars(deviceName, 0);

    openNIContext = new OpenNIContext();
    trackingCamera = new TrackingCamera(openNIContext);
    result = trackingCamera->init(dev, 640, 480);
    env->ReleaseStringUTFChars(deviceName, dev);

    return result;
}

jint readFrame(JNIEnv *env, jobject instance, jobject result) {
    bool objectFound = false;

    jclass res = env->GetObjectClass(result);
    jmethodID getFrameAddrID = env->GetMethodID(res, "getFrameAddr", "()J");
    jfieldID objectFoundID = env->GetFieldID(res, "objectFound", "Z");

    Mat &mat = *(Mat *) env->CallLongMethod(result, getFrameAddrID);

    trackingCamera->readFrame(mat, objectFound);

    env->SetBooleanField(result, objectFoundID, static_cast<jboolean>(objectFound));
    return 0;
}

jint erodeBy(JNIEnv *env, jobject instance, jint erodeBy) {
    _erodeBy = erodeBy;
    return 0;
}

jint dilateBy(JNIEnv *env, jobject instance, jint dilateBy) {
    _dilateBy = dilateBy;
    return 0;
}

jint blurBy(JNIEnv *env, jobject instance, jint blurBy) {
    _blurBy = blurBy;
    return 0;
}

jint teardown(JNIEnv *env, jobject instance) {
    if (trackingCamera != nullptr) {
        trackingCamera->release();
    }
    if (openNIContext != nullptr) {
        openNIContext->release();
    }
    return 0;
}

// Class registration
JNINativeMethod jniMethods[] = {
        {"init",      "(Ljava/lang/String;)I",                      (void *) &init},
        {"erodeBy",   "(I)I",                                       (void *) &erodeBy},
        {"dilateBy",  "(I)I",                                       (void *) &dilateBy},
        {"blurBy",    "(I)I",                                       (void *) &blurBy},
//        {"readFrame", "(J)I",                  (void *) &readFrame},
        {"readFrame", "(Lcom/axis4/android/jni/ReadFrameResult;)I", (void *) &readFrame},
        {"teardown",  "()I",                                        (void *) &teardown},
};

jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNIEnv *env;

    if (vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }

    jclass clz = env->FindClass(REGISTER_CLASS);
    env->NewGlobalRef(clz);
    env->RegisterNatives(clz, jniMethods, sizeof(jniMethods) / sizeof(JNINativeMethod));
    env->DeleteLocalRef(clz);
    LOGD("ObjectTracker JNI_OnLoad");
    return JNI_VERSION_1_6;
}

void JNI_OnUnLoad(JavaVM *vm, void *reserved) {
    LOGD("ObjectTracker JNI_OnUnLoad");
}