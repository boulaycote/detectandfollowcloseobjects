#ifndef DETECTANDFOLLOWCLOSEOBJECTS_TRACKINGCAMERA_H
#define DETECTANDFOLLOWCLOSEOBJECTS_TRACKINGCAMERA_H

#include "OpenNICamera.h"
#include "DepthCamera.h"
#include "IRCamera.h"
#include "DetectedObject.h"
#include "BlobTracker.h"
#include "RGBCamera.h"

using namespace std;

class TrackingCamera {

    OpenNIContext *openNIContext;
    OpenNICamera *depthCamera;
    OpenNICamera *irCamera;
    OpenNICamera *rgbCamera;
//    RGBCamera *rgbCamera;


//    Ptr<BackgroundSubtractorMOG2> subtractor;
//    Mat mask;

    BlobTracker *blobTracker;

    bool
    detectObjects(Mat &original, Mat &gray, Mat &depth, vector<DetectedObject> detectedObjects);

    Mat trackObjects(OpenNICamera *camera);

public:
    TrackingCamera(OpenNIContext *openNIContext);

    int init(const char *deviceName, int width, int height);

    void release();

    int readFrame(Mat &frame, bool &objectFound);
};


#endif //DETECTANDFOLLOWCLOSEOBJECTS_TRACKINGCAMERA_H
