#ifndef DETECTANDFOLLOWCLOSEOBJECTS_DETECTEDOBJECT_H
#define DETECTANDFOLLOWCLOSEOBJECTS_DETECTEDOBJECT_H

struct DetectedObject {
    int id;

    int x;

    int y;

    int width;

    int height;

    double distance;

    DetectedObject(int id, int x, int y, int width, int height, double distance) {
        this->id = id;
        this->x = x;
        this->y = y;
        this->width = width;
        this->height = height;
        this->distance = distance;

//        LOGD("(%d, %d) %dx%d at %g", x, y, width, height, distance);
    }


};

#endif //DETECTANDFOLLOWCLOSEOBJECTS_DETECTEDOBJECT_H
