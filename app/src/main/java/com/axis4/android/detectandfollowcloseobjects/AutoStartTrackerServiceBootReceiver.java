package com.axis4.android.detectandfollowcloseobjects;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import timber.log.Timber;

public class AutoStartTrackerServiceBootReceiver extends WakefulBroadcastReceiver {
  @Override
  public void onReceive (Context context, Intent intent) {
    Timber.d("Received boot intent. Starting TrackerService.");

    context.startService(new Intent(context, TrackerService.class));
  }
}
