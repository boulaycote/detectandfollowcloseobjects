package com.axis4.android.detectandfollowcloseobjects;

import android.content.Context;
import android.os.HandlerThread;
import android.os.SystemClock;

import com.axis4.android.jni.ObjectTracker;
import com.axis4.android.jni.ReadFrameResult;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import timber.log.Timber;

public class TrackerThread extends HandlerThread {

  private static final int SECONDS_BEFORE_EVENT = 1;

  private final Context context;

  private final UsbDeviceListAccessBroker usbDeviceAccessBroker;

  private Listener listener;

  private boolean objectFound;

  public TrackerThread (Context context, Listener listener) {
    super("TrackerThread");

    this.context = context;
    this.listener = listener;

    // TODO Handle multiple kinds of devices.

    usbDeviceAccessBroker = new UsbDeviceListAccessBroker(context,
        Arrays.asList(
            new UsbAccessRequest(11205, 1027), // Astra Pro IR + Depth
            new UsbAccessRequest(11205, 1281), // Astra Pro RGB

            new UsbAccessRequest(11205, 1028)  // Astra Mini
        ));
  }

  @Override
  public void run () {
    ObjectTracker objectTracker = new ObjectTracker();
    long FPS, startTime, countdown = 0, frameCount = 0;
    int framesWithObject = 0;

    try {
      Timber.i("Starting tracker thread.");

      OpenCVInitializer.getInstance().initialize(context);
      Timber.i("OpenCV initialized.");

      if (!usbDeviceAccessBroker.requestAccess()) {
        Timber.e("Could not get USB access.");
      } else {
        Timber.i("Got USB access.");

        Timber.i("Starting tracker.");
        if (!objectTracker.initialize()) {
          Timber.e("Could not initialize tracker.");
        } else {
          startTime = SystemClock.elapsedRealtime();

          while (!interrupted()) {
            try {
              ReadFrameResult result = objectTracker.readFrame();
              long elapsedTime = SystemClock.elapsedRealtime() - startTime;

              FPS = ++frameCount * 1000 / elapsedTime; // * 1000 because ms

              if (result.isObjectFound()) {
                if (++framesWithObject >= SECONDS_BEFORE_EVENT * FPS) {
                  if (!objectFound) { // Found object once
                    listener.onObjectFound(framesWithObject / FPS);
                  }
                  objectFound = true;
                  countdown = SECONDS_BEFORE_EVENT * FPS;
                }
              } else if (objectFound && --countdown == 0) { // object was found, it's gone now.
                objectFound = false;
                listener.onObjectLeft(framesWithObject / FPS);
                framesWithObject = 0;
              }

              if (listener != null) {
                listener.onFrameRead(result);
              } else {
                result.release();
              }
            } catch (TimeoutException ex) {

              // TODO There might be a problem, like two trackers opened or something.

              Timber.e("ReadFrame timed out.");
            }
          }
        }
      }
    } catch (InterruptedException e) {
      Timber.w("Tracker thread interrupted.");
    } catch (ExecutionException e) {
      Timber.e(e, "Tracker thread encountered an issue.");
    }

    objectTracker.release();
    usbDeviceAccessBroker.release();

    Timber.i("Tracker thread stopped.");
  }

  interface Listener {
    void onFrameRead (ReadFrameResult result);

    void onObjectFound (long presentForSec);

    void onObjectLeft (long presentForSec);
  }
}
