package com.axis4.android.detectandfollowcloseobjects.views;

import android.content.Context;
import android.graphics.Color;
import android.opengl.GLES10;
import android.opengl.GLES11;
import android.opengl.GLES11Ext;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import org.opencv.core.Mat;

import java.nio.ByteBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


public class FrameRenderView extends GLSurfaceView {

  protected int surfaceWidth = 0;

  protected int surfaceHeight = 0;

  protected ByteBuffer texture;

  protected int textureId = 0;

  private long nativePtr = 0;

  private int currentFrameWidth = 0;

  private int currentFrameHeight = 0;

  public FrameRenderView (Context context) {
    super(context);
    init();
  }

  public FrameRenderView (Context context, AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  private void init () {

    setRenderer(new Renderer() {

      @Override
      public void onSurfaceCreated (GL10 gl, EGLConfig c) {
        // Disable these capabilities.
        final int gCapbilitiesToDisable[] = {
            GLES10.GL_FOG,
            GLES10.GL_LIGHTING,
            GLES10.GL_CULL_FACE,
            GLES10.GL_ALPHA_TEST,
            GLES10.GL_BLEND,
            GLES10.GL_COLOR_LOGIC_OP,
            GLES10.GL_DITHER,
            GLES10.GL_STENCIL_TEST,
            GLES10.GL_DEPTH_TEST,
            GLES10.GL_COLOR_MATERIAL,
        };

        for (int capability : gCapbilitiesToDisable) {
          GLES10.glDisable(capability);
        }

        GLES10.glEnable(GLES10.GL_TEXTURE_2D);

        int ids[] = new int[1];
        GLES10.glGenTextures(1, ids, 0);
        textureId = ids[0];
        GLES10.glBindTexture(GLES10.GL_TEXTURE_2D, textureId);

        GLES10.glTexParameterf(GLES10.GL_TEXTURE_2D, GLES10.GL_TEXTURE_MIN_FILTER, GLES10.GL_LINEAR);
        GLES10.glTexParameterf(GLES10.GL_TEXTURE_2D, GLES10.GL_TEXTURE_MAG_FILTER, GLES10.GL_LINEAR);
        GLES10.glShadeModel(GLES10.GL_FLAT);
      }

      @Override
      public void onSurfaceChanged (GL10 gl, int w, int h) {
        synchronized (FrameRenderView.this) {
          surfaceWidth = w;
          surfaceHeight = h;
        }
      }

      @Override
      public void onDrawFrame (GL10 gl) {
        synchronized (FrameRenderView.this) {
          onDrawGL();
        }
      }
    });

    setRenderMode(RENDERMODE_WHEN_DIRTY);
  }

  @Override
  protected void finalize () throws Throwable {
    if (nativePtr != 0) {
      nativePtr = 0;
    }
    super.finalize();
  }

  synchronized public void update (Mat frame) {
    int size = frame.rows() * frame.cols() * frame.channels();
    byte[] bytes = new byte[size];

    currentFrameWidth = frame.width();
    currentFrameHeight = frame.height();

    if (texture == null) {
      texture = ByteBuffer.allocateDirect(size);
    }
    frame.get(0, 0, bytes);
    texture = ByteBuffer.wrap(bytes);

    requestRender();
  }

  protected void onDrawGL () {
    if (texture == null || surfaceWidth == 0 || surfaceHeight == 0) {
      return;
    }
    int baseColor = Color.WHITE;
    int red = Color.red(baseColor);
    int green = Color.green(baseColor);
    int blue = Color.blue(baseColor);
    int alpha = Color.alpha(baseColor);
    int rect[] = {0, currentFrameHeight, currentFrameWidth, -currentFrameHeight};

    GLES10.glEnable(GLES10.GL_BLEND);
    GLES10.glBlendFunc(GLES10.GL_SRC_ALPHA, GLES10.GL_ONE_MINUS_SRC_ALPHA);
    GLES10.glColor4f(red / 255.f, green / 255.f, blue / 255.f, alpha / 255.f);
    GLES10.glEnable(GLES10.GL_TEXTURE_2D);
    GLES10.glBindTexture(GLES10.GL_TEXTURE_2D, textureId);
    GLES11.glTexParameteriv(GLES10.GL_TEXTURE_2D, GLES11Ext.GL_TEXTURE_CROP_RECT_OES, rect, 0);
    GLES10.glClear(GLES10.GL_COLOR_BUFFER_BIT);
    GLES10.glTexImage2D(GLES10.GL_TEXTURE_2D, 0, GLES10.GL_RGBA, currentFrameWidth, currentFrameHeight, 0, GLES10.GL_RGBA,
        GLES10.GL_UNSIGNED_BYTE, texture);
    GLES11Ext.glDrawTexiOES(0, 0, 0, surfaceWidth, surfaceHeight);
    GLES10.glDisable(GLES10.GL_TEXTURE_2D);
  }
}
