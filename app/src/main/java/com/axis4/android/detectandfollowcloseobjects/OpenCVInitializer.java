package com.axis4.android.detectandfollowcloseobjects;

import android.content.Context;

import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.util.concurrent.CountDownLatch;

import timber.log.Timber;

public class OpenCVInitializer {

  static OpenCVInitializer instance;

  private boolean initialized;

  private Listener currentListener;

  private CountDownLatch latch;

  private LoaderCallbackInterface asyncLoaderCallback = new OpenCVLoaderCallbackInterface() {
    @Override
    public void onManagerConnected (int status) {
      Timber.d("OpenCV Loaded. Request USB access.");

      initialized = true;

      if (currentListener != null) {
        currentListener.onCompleted();
      }
    }
  };

  private LoaderCallbackInterface syncLoaderCallback = new OpenCVLoaderCallbackInterface() {
    @Override
    public void onManagerConnected (int status) {
      latch.countDown();
    }
  };

  static OpenCVInitializer getInstance () {
    if (instance == null) {
      instance = new OpenCVInitializer();
    }
    return instance;
  }

  public void initialize (Context context, Listener listener) {
    currentListener = listener;

    if (!initialized) {
      if (!OpenCVLoader.initDebug()) {
        Timber.i("Internal OpenCV library not found. Using OpenCV Manager for initialization");
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, context, asyncLoaderCallback);
      } else {
        Timber.i("OpenCV library found inside package. Using it!");
        asyncLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
      }
    } else {
      Timber.i("OpenCV already initialized.");
      currentListener.onCompleted();
    }
  }

  public void initialize (Context context) throws InterruptedException {
    if (!initialized) {
      if (!OpenCVLoader.initDebug()) {
        Timber.i("Internal OpenCV library not found. Using OpenCV Manager for initialization");
        latch = new CountDownLatch(1);
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, context, syncLoaderCallback);
        latch.await();
      } else {
        Timber.i("OpenCV library found inside package. Using it!");
      }
    }
  }

  public interface Listener {
    void onCompleted ();
  }

}
