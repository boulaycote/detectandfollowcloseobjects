package com.axis4.android.detectandfollowcloseobjects;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.axis4.android.detectandfollowcloseobjects.views.FrameRenderView;
import com.axis4.android.jni.ReadFrameResult;

import java.lang.ref.WeakReference;

import timber.log.Timber;

public class MainActivity extends FullScreenAppCompatActivity {

  // Views
  private FrameRenderView renderView;


  private TrackerService.ServiceBinder trackerServiceBinder;

  private boolean boundToService = false;

  private ServiceConnection trackerServiceConnection = new ServiceConnection() {
    @Override
    public void onServiceConnected (ComponentName name, IBinder binder) {
      Timber.d("Tracker Service connected.");
    }

    @Override
    public void onServiceDisconnected (ComponentName name) {
      Timber.d("Tracker Service disconnected.");
    }
  };

  private BroadcastReceiver objectFoundBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive (Context context, Intent intent) {
      Toast.makeText(getApplicationContext(), "Something found!", Toast.LENGTH_SHORT).show();
    }
  };

  private BroadcastReceiver objectGoneBroadcastReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive (Context context, Intent intent) {
      long time = intent.getLongExtra(TrackerService.OBJECT_GONE_BROADCAST_TIME_EXTRA, 0);
      String message = String.format("Something is gone stayed for %s seconds.", time);

      Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
  };

  @Override
  protected void onCreate (Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    renderView = findViewById(R.id.renderView);

    Button startServiceButton = findViewById(R.id.startServiceButton);
    startServiceButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick (View v) {
        startTrackerService();
      }
    });

    Button stopServiceButton = findViewById(R.id.stopServiceButton);
    stopServiceButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick (View v) {
        stopTrackerService();
      }
    });
  }

  @Override
  protected void onDestroy () {
    super.onDestroy();

    unbindService();
  }

  @Override
  protected void onPause () {
    super.onPause();

    unregisterReceiver(objectFoundBroadcastReceiver);
    unregisterReceiver(objectGoneBroadcastReceiver);
  }

  @Override
  protected void onResume () {
    super.onResume();

    registerReceiver(objectFoundBroadcastReceiver, new IntentFilter(TrackerService.OBJECT_FOUND_BROADCAST));
    registerReceiver(objectGoneBroadcastReceiver, new IntentFilter(TrackerService.OBJECT_GONE_BROADCAST));
  }

  private void startTrackerService () {
    Intent intent = new Intent(getApplicationContext(), TrackerService.class);
    Messenger messenger = new Messenger(new ServiceMessageHandler(this));

    intent.putExtra(TrackerService.EXTRA_MESSENGER, messenger);
    if (bindService(intent, trackerServiceConnection, Context.BIND_AUTO_CREATE)) {
      boundToService = true;
      Timber.i("Bound to service.");
    } else {
      Timber.e("Could not bind to service.");
    }
    startService(intent);
  }

  private void stopTrackerService () {
    Intent intent = new Intent(getApplicationContext(), TrackerService.class);

    unbindService();
    stopService(intent);
  }

  private void unbindService () {
    if (boundToService) {
      Timber.i("Unbinding from service.");
      unbindService(trackerServiceConnection);
      boundToService = false;
    } else {
      Timber.i("Not bound to service.");
    }
  }

  private void handleServiceMessage (Message msg) {
    ReadFrameResult result = (ReadFrameResult) msg.obj;

    if (result != null) {
      renderView.update(result.getFrame());

      // When there's a messenger the frame is not released automatically. Release it here.
      result.release();
    }
  }

  private static class ServiceMessageHandler extends Handler {

    private final WeakReference<MainActivity> activity;

    public ServiceMessageHandler (MainActivity activity) {
      this.activity = new WeakReference<>(activity);
    }

    @Override
    public void handleMessage (Message msg) {
      MainActivity activity = this.activity.get();

      if (activity != null) {
        activity.handleServiceMessage(msg);
      }
    }
  }
}
