package com.axis4.android.detectandfollowcloseobjects;

public class UsbAccessRequest {

  private int vendorId;

  private int productId;

  public UsbAccessRequest(int vendorId, int productId) {
    this.vendorId = vendorId;
    this.productId = productId;
  }

  public int getVendorId () {
    return vendorId;
  }

  public int getProductId () {
    return productId;
  }

  @Override
  public String toString () {
    return String.format("vendorId: %s, productId: %s", vendorId, productId);
  }
}
