package com.axis4.android.detectandfollowcloseobjects;

import org.opencv.android.InstallCallbackInterface;
import org.opencv.android.LoaderCallbackInterface;

public abstract class OpenCVLoaderCallbackInterface implements LoaderCallbackInterface {
  @Override
  public void onManagerConnected (int status) {

  }

  @Override
  public void onPackageInstall (int operation, InstallCallbackInterface callback) {

  }
}
