package com.axis4.android.detectandfollowcloseobjects;

import org.openni.PixelFormat;

public class C {
  public static final int RES_COLOR_WIDTH = 640;

  public static final int RES_COLOR_HEIGHT = 480;

  public static final VideoModeOptions IR_VIDEO_MODE_OPTIONS =
      new VideoModeOptions(RES_COLOR_WIDTH, RES_COLOR_HEIGHT, PixelFormat.RGB888);
}
