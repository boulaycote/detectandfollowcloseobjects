package com.axis4.android.detectandfollowcloseobjects;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import timber.log.Timber;

public class UsbDeviceAccessBroker {

  private final Context context;

  private final UsbManager usbManager;

  private final RandomString random;

  private HashMap<String, BroadcastReceiver> broadcastReceivers = new HashMap<>();

  // Maintain a connection list to dispose of them when released.
  private List<UsbDeviceConnection> usbDeviceConnectionList = new ArrayList<>();

  public UsbDeviceAccessBroker (Context context) {
    this.context = context;
    this.usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
    this.random = new RandomString();
  }

  @NonNull
  private String getNextReceiverKey () {
    return String.format("%s.USB_PERMISSION.%s", context.getPackageName(), random.nextString());
  }

  public void requestAccess (UsbAccessRequest usbAccessRequest, Listener listener) {
    PendingIntent permissionIntent;
    Collection<UsbDevice> devices = usbManager.getDeviceList().values();
    String receiverKey;
    boolean deviceFound = false;

    for (UsbDevice device : devices) {
      if (device.getVendorId() == usbAccessRequest.getVendorId() &&
          device.getProductId() == usbAccessRequest.getProductId()) {
        receiverKey = getNextReceiverKey();
        deviceFound = true;
        registerReceiver(receiverKey, listener);
        permissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(receiverKey), 0);
        usbManager.requestPermission(device, permissionIntent);
        break;
      }
    }

    if (!deviceFound) {
      listener.onCompleted(false);
    }
  }

  public void release () {
    unregisterAllReceivers();

    for (UsbDeviceConnection deviceConnection : usbDeviceConnectionList) {
      deviceConnection.close();
    }
  }

  private void registerReceiver (final String receiverKey, final Listener listener) {
    BroadcastReceiver receiver;

    if (broadcastReceivers.containsKey(receiverKey)) {
      unregisterReceiver(receiverKey);
    }

    receiver = new BroadcastReceiver() {
      public void onReceive (Context context, Intent intent) {
        String action = intent.getAction();

        if (receiverKey.equals(action)) {
          boolean granted = intent.getBooleanExtra("permission", false);
          UsbDevice device = intent.getParcelableExtra("device");

          if (granted && device != null) {
            UsbDeviceConnection deviceConnection = usbManager.openDevice(device);

            granted = deviceConnection != null;
            if (granted) {
              usbDeviceConnectionList.add(deviceConnection);
            }
          }
          listener.onCompleted(granted);
          unregisterReceiver(receiverKey);
        }
      }
    };
    context.registerReceiver(receiver, new IntentFilter(receiverKey));
    broadcastReceivers.put(receiverKey, receiver);
    Timber.d("Receiver registered with key %s.", receiverKey);
  }

  private void unregisterReceiver (String receiverKey) {
    BroadcastReceiver receiver = broadcastReceivers.get(receiverKey);

    if (receiver != null) {
      context.unregisterReceiver(receiver);
      broadcastReceivers.remove(receiverKey);
      Timber.d("Receiver with key %s unregistered.", receiverKey);
    }
  }

  private void unregisterAllReceivers () {
    Collection<BroadcastReceiver> receivers = broadcastReceivers.values();

    for (BroadcastReceiver receiver : receivers) {
      context.unregisterReceiver(receiver);
    }
  }

  public interface Listener {
    void onCompleted (boolean success);
  }
}
