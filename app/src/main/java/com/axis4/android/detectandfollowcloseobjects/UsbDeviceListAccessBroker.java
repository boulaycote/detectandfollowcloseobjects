package com.axis4.android.detectandfollowcloseobjects;

import android.content.Context;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

public class UsbDeviceListAccessBroker {

  private UsbDeviceAccessBroker usbDeviceAccessBroker;

  private Queue<UsbAccessRequest> usbAccessRequestQueue;

  private CountDownLatch latch;

  public UsbDeviceListAccessBroker (Context context, List<UsbAccessRequest> usbAccessRequestList) {
    usbDeviceAccessBroker = new UsbDeviceAccessBroker(context);
    usbAccessRequestQueue = new LinkedList<>(usbAccessRequestList);
  }

  public void requestAccess (final Listener listener) {
    UsbAccessRequest request = usbAccessRequestQueue.poll();

    if (request != null) {
      usbDeviceAccessBroker.requestAccess(request, new UsbDeviceAccessBroker.Listener() {
        @Override
        public void onCompleted (boolean granted) {
          if (granted) {
            requestAccess(listener);
          } else {
            listener.onCompleted(false);
          }
        }
      });
    } else {
      listener.onCompleted(true);
    }
  }

  public boolean requestAccess () throws InterruptedException {
    final AtomicBoolean success = new AtomicBoolean(true);
    UsbAccessRequest request;

    for (int i = 0; i < usbAccessRequestQueue.size(); i++) {
      if (success.get()) {
        request = usbAccessRequestQueue.poll();
        latch = new CountDownLatch(1);
        usbDeviceAccessBroker.requestAccess(request, new UsbDeviceAccessBroker.Listener() {
          @Override
          public void onCompleted (boolean granted) {
            success.set(granted);
            latch.countDown();
          }
        });
        latch.await();
      }
    }
    return success.get();
  }

  public void release () {
    usbDeviceAccessBroker.release();
  }

  public interface Listener {
    void onCompleted (boolean success);
  }
}
