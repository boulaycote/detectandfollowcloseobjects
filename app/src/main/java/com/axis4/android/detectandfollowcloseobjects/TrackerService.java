package com.axis4.android.detectandfollowcloseobjects;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.axis4.android.jni.ReadFrameResult;

import timber.log.Timber;

public class TrackerService extends Service implements TrackerThread.Listener {

  public static final String EXTRA_MESSENGER = "messenger";

  public static final String OBJECT_FOUND_BROADCAST = "com.axis4.android.OBJECT_FOUND";

  public static final String OBJECT_GONE_BROADCAST = "com.axis4.android.OBJECT_GONE";

  public static final String OBJECT_GONE_BROADCAST_TIME_EXTRA = "com.axis4.android.OBJECT_GONE_TIME_EXTRA";

  private static int NOTIFICATION = R.string.tracker_service_started;

  private final IBinder binder = new ServiceBinder();

  private NotificationManager notificationManager;

  private Messenger messenger;

  private TrackerThread trackerThread;

  private boolean trackerStarted;

  @Override
  public void onCreate () {
    super.onCreate();

    notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    showNotification();
  }

  @Override
  public int onStartCommand (Intent intent, int flags, int startId) {
    Timber.i("Starting service.");

    if (!trackerStarted) {
      trackerThread = new TrackerThread(getApplicationContext(), this);
      trackerThread.start();
      trackerStarted = true;
    }
    return Service.START_STICKY;
  }

  @Override
  public void onDestroy () {
    super.onDestroy();

    Timber.i("Stopping service.");

    if (trackerThread != null) {
      try {
        trackerThread.interrupt();
        trackerThread.join();
      } catch (InterruptedException e) {
        Timber.w("Tracker thread shutdown interrupted.");
      }
    }
    trackerStarted = false;

    notificationManager.cancel(NOTIFICATION);
  }

  @Nullable
  @Override
  public IBinder onBind (Intent intent) {
    Bundle extras = intent.getExtras();

    if (extras != null) {
      messenger = (Messenger) extras.get(EXTRA_MESSENGER);
    }
    Timber.i("Service bound.");
    return binder;
  }

  private void showNotification () {
    CharSequence text = getText(R.string.tracker_service_started);

    PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
        new Intent(this, MainActivity.class), 0);

    Notification notification = new Notification.Builder(this)
        .setSmallIcon(R.mipmap.ic_launcher_round)
        .setTicker(text)
        .setWhen(System.currentTimeMillis())
        .setContentTitle(getText(R.string.tracker_service_label))
        .setContentText(text)
        .setContentIntent(contentIntent)
        .build();

    notificationManager.notify(NOTIFICATION, notification);
  }

  @Override
  public void onFrameRead (ReadFrameResult result) {
    if (messenger != null) {
      sendResult(result);
    } else {
      result.release();
    }
  }

  @Override
  public void onObjectFound (long presentForSec) {
    Intent intent = new Intent();

    intent.setAction(OBJECT_FOUND_BROADCAST);
    // TODO intent.putExtra(number of objects, tracked of objects);
    sendBroadcast(intent);

    Timber.i("Object present for %s seconds.", presentForSec);
  }

  @Override
  public void onObjectLeft (long presentForSec) {
    Intent intent = new Intent();

    intent.setAction(OBJECT_GONE_BROADCAST);
    intent.putExtra(OBJECT_GONE_BROADCAST_TIME_EXTRA, presentForSec);
    sendBroadcast(intent);

    Timber.i("Object left. Stayed for %s seconds.", presentForSec);
  }

  private void sendResult (ReadFrameResult result) {
    Message message = Message.obtain();

    try {
      message.obj = result;
      messenger.send(message);
    } catch (RemoteException e) {
      e.printStackTrace();
    }
  }

  class ServiceBinder extends Binder {
    TrackerService getService () {
      return TrackerService.this;
    }
  }
}
