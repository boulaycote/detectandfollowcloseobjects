package com.axis4.android.detectandfollowcloseobjects;

import org.openni.PixelFormat;
import org.openni.VideoMode;

public class VideoModeOptions {

  private int width;

  private int height;

  private PixelFormat pixelFormat;

  public VideoModeOptions (int width, int height, PixelFormat pixelFormat) {
    this.width = width;
    this.height = height;
    this.pixelFormat = pixelFormat;
  }

  public int getWidth () {
    return width;
  }

  public int getHeight () {
    return height;
  }

  public PixelFormat getPixelFormat () {
    return pixelFormat;
  }

  public boolean matches (VideoMode videoMode) {
    int width = videoMode.getResolutionX();
    int height = videoMode.getResolutionY();
    PixelFormat pixelFormat = videoMode.getPixelFormat();

    return width == getWidth() &&
        height == getHeight() &&
        pixelFormat == getPixelFormat();
  }
}
