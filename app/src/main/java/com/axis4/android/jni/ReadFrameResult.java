package com.axis4.android.jni;

import org.opencv.core.Mat;

public class ReadFrameResult {

  private Mat frame;

  private long frameAddr;

  private boolean objectFound;

  public ReadFrameResult () {
    this(new Mat(), false);
  }

  public ReadFrameResult (Mat frame, boolean objectFound) {
    this.frame = frame;
    this.frameAddr = this.frame.getNativeObjAddr();
    this.objectFound = objectFound;
  }

  public Mat getFrame () {
    return frame;
  }

  public boolean isObjectFound () {
    return objectFound;
  }

  public long getFrameAddr () {
    return frameAddr;
  }

  public void release () {
    if (frame != null) {
      frame.release();
    }
  }
}
