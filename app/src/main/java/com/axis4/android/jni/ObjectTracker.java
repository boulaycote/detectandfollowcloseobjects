package com.axis4.android.jni;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ObjectTracker {

  private static final String TAG = "ObjectTracker";

  private static final String DEFAULT_CAMERA = "/dev/video0";

  private static final int READ_FRAME_TIMEOUT = 1000;

  static {
    System.loadLibrary(TAG);
  }

  private final ExecutorService threadPool;

  private String deviceName;

  private boolean initialized = false;

  public ObjectTracker () {
    this(DEFAULT_CAMERA);
  }

  public ObjectTracker (String deviceName) {
    this.deviceName = deviceName;
    this.threadPool = Executors.newSingleThreadExecutor();
  }

  private native static int init (String deviceName);

  private native static int readFrame (ReadFrameResult result);

  private native static int erodeBy (int erodeBy);

  private native static int dilateBy (int dilateBy);

  private native static int blurBy (int blurBy);

  private native static int teardown ();

  public boolean initialize () {
    if (!initialized) {
      initialized = init(deviceName) == 0;
    }
    return initialized;
  }

  public void release () {
    initialized = false;
    teardown();
  }

  public ReadFrameResult readFrame () throws InterruptedException, ExecutionException, TimeoutException {
    return readFrame(READ_FRAME_TIMEOUT);
  }

  public void setErode (int by) {
    erodeBy(by);
  }

  public void setDilate (int by) {
    dilateBy(by);
  }

  public void setBlur (int by) {
    blurBy(by);
  }

  private ReadFrameResult readFrame (long timeout) throws InterruptedException, ExecutionException, TimeoutException {
    Future<ReadFrameResult> read = threadPool.submit(new Reader());

    return read.get(timeout, TimeUnit.MILLISECONDS);
  }

  private class Reader implements Callable<ReadFrameResult> {

    private ReadFrameResult result = new ReadFrameResult();

    @Override
    public ReadFrameResult call () {
      readFrame(result);
      return result;
    }
  }
}
